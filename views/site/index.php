<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">
    <h1><?= Html::encode($this->title) //colocamos el título a la vista con el nombre del eror y el código?></h1>
    
    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) //colocamos el mensaje de error?>
    </div>
    <p> Upss....</p>
    <p>
        Hay un problema
    </p>
    
</div>
